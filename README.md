O SigaRegistro é um app de registro de ponto simples.

Com interface simples e intuitiva o administrador acessa a rota *Raiz* do projeto e pode cadastrar novos usuários. 
Por sua vêz o usuário acessa a rota *Registrar* e pode inserir o *CPF* cadastrado para registrar seu ponto.

Siim, precisa desenvolver as restrições de acesso do usuário e colocar a rota raiz para exibir 
a rota *Registrar* no canto superior direito na view de *Registro* tem um botão que leva para a view de Login do administrador.

A rota *Raiz* do projeto chama direto a lista de usuários cadastrados.

A rota *Registrar* chama a view para o usuário registrar o ponto do horário.

Para buscar *buscar* o registro de ponto de um usuário só precisa digitar o nome dele no Search na view principal do repositório, criei um botão na linha com as informações do usuário mas nao ta carregando os dados do usuário dentro da view de Registros de Ponto.

Banco de Dados

Name: sigaponto
Tables: usuarios / registros

CREATE TABLE `usuarios` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL DEFAULT '',
  `cpf` varchar(14) NOT NULL DEFAULT '',
  `registrado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `nascimento` date NOT NULL,
  `setor` int(3) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=126 DEFAULT CHARSET=utf8;

CREATE TABLE `registros` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cpf` varchar(12) NOT NULL DEFAULT '',
  `nome` varchar(50) DEFAULT NULL,
  `nascimento` date NOT NULL,
  `horario` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=utf8;
