<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="registro">
        <ul class="nav justify-content-end">
            <li class="nav-item">
                <a class="nav-link active" href="../">Login</a>
            </li>
        </ul>
    <div class="container" style="padding-top:15%;">
        <div class="row">
            <div class="col col-md-3"></div>
                <div class="col col-md-6" style="margin-bottom: 60px;">
                    <form name="form" action="../retorno/" method="post">
                        <div class="form-group">
                            <div class="form-group">
                                <h1 >Registro</h1>
                            </div>
                            <input type="number" class="form-control form-control-lg" id="cpf" name="cpf" placeholder="Digite o seu CPF" maxlength="111">
                        </div>
                        <button type="submit" class="btn btn-info btn-lg float-right">Registrar</button>
                    </form>
                </div>
            <div class="col col-md-3"></div>
        </div>
    </div>
</div>