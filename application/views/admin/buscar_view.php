<div class="buscar">
    <!-- Image and text -->
    <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="#">
            <img src="https://getbootstrap.com/docs/4.1/assets/brand/bootstrap-solid.svg" width="30" height="30" class="d-inline-block align-top" alt="">
            SigaRegistro
        </a>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col"></div>
            <div class="col-10">
                <h1 class="bd-title" style="padding-top:15px;">Histórico de Registro</h1> 
                <hr size="3">
                <a href="../" class="btn btn-light" style="margin-bottom:15px;"><i class="fas fa-arrow-left"></i></a>
                <a href="" class="btn btn-info" style="margin-bottom:15px;" onclick="imprimir()">Imprimir</a>
                <br>

                <table class="table table-hover">
                    <thead>
                        <tr>
                        <th scope="col" class="text-center">CPF</th>
                        <th scope="col" class="text-center">Nome</th>
                        <th scope="col" class="text-center">Nascimento</th>
                        <th scope="col" class="text-center">Horário</th>
                        </tr>
                    </thead>
                    <tbody> 
                    <?php 
                            foreach($usuarios as $usuario)
                            {
                                echo '<tr>';
                                    echo '<th scope="row" class="text-center">'.$usuario->cpf.'</th>';
                                    echo '<td class="text-center">'.$usuario->nome.'</td>';
                                    echo '<td class="text-center">'.$usuario->nascimento.'</td>';
                                    echo '<td class="text-center">'.$usuario->horario.'</td>';
                                    /* echo '<td class="text-center">';
                                        if ($usuario->setor == 1) 
                                        {
                                            echo 'Callcenter';
                                        }
                                        else if($usuario->setor == 2)
                                        {
                                            echo 'Administrativo';
                                        }
                                        else if($usuario->setor == 3)
                                        {
                                            echo 'TI';
                                        }
                                        else if($usuario->setor == 4)
                                        {
                                            echo 'Serviços Gerais';
                                        }
                                        else if($usuario->setor = 5)
                                        {
                                            echo 'Faturamento';
                                        }
                                    '</td>'; */
                                    /* echo '<td class="text-center">';
                                        if ($usuario->status == 1) 
                                        {
                                            echo 'ATIVO';
                                        }
                                        else{
                                            echo 'DESATIVADO';
                                        }
                                    echo'</td>'; */
                                    /* echo '<td>';
                                        echo '<div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups" >';
                                            echo '<div class="btn-group mr-2" role="group" aria-label="First group">';
                                                echo '<button type="submit" class="btn btn-light"><a href="admin/go/editarUsuario/'.$usuario->id.'"><i class="far fa-address-card"></i></a></button>';
                                            echo '</div>';
                                        echo '</div>';
                                    echo '</td>'; */
                                echo '</tr>';
                            }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="col"></div>
        </div>
    </div>
</div>
