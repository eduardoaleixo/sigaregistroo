<div class="login">
    <!-- Image and text -->
    <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="/">
            <img src="https://getbootstrap.com/docs/4.1/assets/brand/bootstrap-solid.svg" width="30" height="30" class="d-inline-block align-top" alt="">
            SigaRegistro
        </a>
    </nav>
    <div class="container-fluid">
        <div class="row">
            <div class="col"></div>
            <div class="col-10">
                <form action="buscarusuario/" method="post">
                    <div class="input-group input-group-lg" style="padding-top:3%;">
                        <div class="dropdown input-group-prepend">
                            <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-bars"></i>
                            </button>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <a class="dropdown-item" href="desativados/"> <i class="fas fa-times"></i> Usuários Desativados</a>
                            </div>
                        </div>
                        <input type="text" name="nome" class="form-control" aria-label="Large" aria-describedby="inputGroup-sizing-sm">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="inputGroup-sizing-lg"><a href="novousuario/">+</a></span>
                        </div>
                    </div>
                </form>
                <br>
                <table class="table table-hover">
                    <thead>
                        <tr>
                        <th scope="col" class="text-center">ID</th>
                        <th scope="col" class="text-center">Nome</th>
                        <th scope="col" class="text-center">Setor</th>
                        <th scope="col" class="text-center">Status</th>
                        <th scope="col" class="text-center">Opções</th>
                        </tr>
                    </thead>
                    <tbody> 
                        <?php
                            foreach($usuarios as $usuario)
                            {
                                if ($usuario->status == 1) 
                                {
                                echo '<tr>';
                                    echo '<th scope="row" class="text-center">'.$usuario->id.'</th>';
                                    echo '<td class="text-center">'.$usuario->nome.'</td>';


                                    echo '<td class="text-center">';
                                        if ($usuario->setor == 1) 
                                        {
                                            echo 'Callcenter';
                                        }
                                        else if($usuario->setor == 2)
                                        {
                                            echo 'Administrativo';
                                        }
                                        else if($usuario->setor == 3)
                                        {
                                            echo 'TI';
                                        }
                                        else if($usuario->setor == 4)
                                        {
                                            echo 'Serviços Gerais';
                                        }
                                        else if($usuario->setor = 5)
                                        {
                                            echo 'Faturamento';
                                        }
                                    '</td>';

                                    echo '<td class="text-center">';
                                    if ($usuario->status == 1) 
                                    {
                                        echo 'ATIVO';
                                    }
                                    else{
                                        echo 'DESATIVADO';
                                    }
                                    echo'</td>';
                                    echo '<td>';
                                        echo '<div class="btn btn-link">';
                                                echo '<a href="admin/go/editarUsuario/'.$usuario->id.'" btn btn-outline-info"><i class="far fa-address-card"></i></a>';
                                        echo '</div>';
                                        echo '<div class="btn btn-link">';
                                            echo '<a href="admin/go/buscar/'.$usuario->nome.'" btn btn-outline-info"><i class="fas fa-list-ul"></i></a>';
                                        echo '</div>';
                                    echo '</td>';
                                echo '</tr>';
                                }
                            }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="col"></div>
        </div>
    </div>
</div>