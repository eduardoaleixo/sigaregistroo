<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="usuario">
    <!-- Image and text -->
    <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="#">
            <img src="https://getbootstrap.com/docs/4.1/assets/brand/bootstrap-solid.svg" width="30" height="30" class="d-inline-block align-top" alt="">
            SigaRegistro
        </a>
    </nav>
        <div class="container">
            <div class="row">
                <div class="col col-md-1"></div>
                <div class="col col-md-10" style="margin-bottom: 60px;">
                    <form name="form" action="../admin/go/salvarUsuario" method="post">
                        <input type="hidden" name="captcha">
                        <div class="form-group">
                            <div class="form-group">
                            <h1 class="bd-title">Novo Usuário</h1>
                            <hr size="3">
                                <label for="nome">Nome</label>
                                <input type="text" class="form-control" name="nome" id="nome" placeholder="Digite o usuário">
                            </div>
                            <label for="cpf">CPF</label>
                            <input type="text" class="form-control" id="cpf" name="cpf" placeholder="Digite o CPF" maxlength="12">
                        </div>
                        <div class="form-group">
                            <label for="cpfconfirmar">Confirmar CPF</label>
                            <input type="text" class="form-control" id="cpfconfirmar" name="cpfconfirmar" placeholder="Digite novamente o CPF" maxlength="12">
                        </div>
                        <div class="form-group">
                            <label for="setor">Setor</label>
                            <select class="custom-select" name="setor" id="setor">
                                <option selected> Defina qual o setor do usuário</option>
                                <option value="1">Callcenter</option>
                                <option value="2">Adminitrativo</option>
                                <option value="3">TI</option>
                                <option value="4">Serviços Gerais</option>
                                <option value="5">Faturamento</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="nascimento">Data de Nascimento</label>
                            <input type="date" class="form-control" id="nascimento" name="nascimento" >
                        </div>
                        <div class="form-group">
                            <label for="">Status Usuário</label>
                            <select class="custom-select" name="status" id="status">
                                <option value="1">Ativado</option>
                                <option value="0">Desativado</option>
                            </select>
                        </div>
                        <button type="submit" id="cadastrar" name="cadastrar" class="btn btn-info" onclick="return confirmarCpf() ">Cadastrar Usuário</button>
                    </form>
                </div>
                <div class="col col-md-1"></div>
            </div>
        </div>
    