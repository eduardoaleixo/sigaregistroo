<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div class="usuario">
    <div class="container">
        <div class="row">
            <div class="col col-md-1"></div>
                <div class="col col-md-10">
                    <form name="form" action="../salvarUsuario" method="post">
                        <input type="hidden" name="captcha">
                        <div class="form-group">
                            <div class="form-group">
                            <h1 class="bd-title">Editar dados do usuário</h1>
                            <hr size="3">
                                <label for="nome">Nome</label>
                                <input type="text" class="form-control" name="nome" id="nome" value="<?php echo $usuario->nome?>" placeholder="Digite o usuário" required>
                            </div>
                            <label for="cpf">CPF</label>
                            <input type="text" class="form-control" id="cpf" name="cpf" value="<?php echo $usuario->cpf ?>" placeholder="Digite o CPF" required maxlength="12">
                            <small id="help" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div class="form-group">
                            <label for="cpfconfirmar">Confirmar CPF</label>
                            <input type="text" class="form-control" id="cpfconfirmar" name="cpfconfirmar" placeholder="Digite novamente o CPF" required maxlength="12">
                            <small id="help" class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                        <div class="form-group">
                            <label for="setor">Setor</label>
                            <select class="custom-select" name="setor" id="setor">
                                <option selected> Defina qual o setor do usuário</option>
                                <option value="1" <?php echo ($usuario->setor == 1 ? 'selected=selected' : '')?>>Callcenter</option>
                                <option value="2" <?php echo ($usuario->setor == 2 ? 'selected=selected' : '')?>>Administrativo</option>
                                <option value="3" <?php echo ($usuario->setor == 3 ? 'selected=selected' : '')?>>TI</option>
                                <option value="4" <?php echo ($usuario->setor == 4 ? 'selected=selected' : '')?>>Serviços Gerais</option>
                                <option value="5" <?php echo ($usuario->setor == 5 ? 'selected=selected' : '')?>>Faturamento</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="nascimento">Data de Nascimento</label>
                            <input type="date" class="form-control" id="nascimento" name="nascimento" value="<?php $usuario->nascimento?>">
                        </div>
                        <div class="form-group">
                            <label for="status">Status Usuário</label>
                        <select class="custom-select" name="status" id="status">
                                <option value="1" <?php echo ($usuario->status == 1 ? 'selected=selected' : '')?>>Ativado</option>
                                <option value="0" <?php echo ($usuario->status == 0 ? 'selected=selected' : '')?>>Desativado</option>
                        </select>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $usuario->id?>">
                        <button type="submit" id="cadastrar" name="cadastrar" class="btn btn-info" onclick="return validar()">Atualizar Dados do Usuário</button>
                    </form>
                </div>
            <div class="col col-md-1"></div>
        </div>
    </div>
</div>
    