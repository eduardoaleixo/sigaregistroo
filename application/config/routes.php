<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller']    = 'welcome';
$route['404_override']          = '';
$route['translate_uri_dashes']  = FALSE;

//-------------------------------ADMIN-------------------------------------------------------------------------------------

$route['listar']                = 'admin/go/listarUsuarios/';
$route['novousuario']           = 'admin/go/adicionarUsuario/';
$route['desativados']           = 'admin/go/usuariosDesativados/';

$route['adicionarUsuario']      = 'admin/go/salvarUsuario/';
$route['atualizarDadosUsuario'] = 'admin/go/salvarDadosUsuario/';

$route['editar']                = 'admin/go/editarUsuario/';
$route['apagarusuario']         = 'admin/go/apagarUsuarioGO/';


//-------------------------------USER-------------------------------------------------------------------------------------

$route['retorno']               = 'usuario/registro/retornoCPF/';
$route['registrarPonto']        = 'usuario/registro/registrarPonto/';
$route['registrar']             = 'usuario/registro/registrar/';

$route['buscarusuario']         = 'admin/go/buscar/';