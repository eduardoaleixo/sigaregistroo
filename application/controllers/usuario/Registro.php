<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registro extends CI_Controller {

    public function retornoCPF()
    {
        $this->load->model('admin/Usuarios_model', 'usuarios');

        $dados['usuario'] = $this->usuarios->usuarioCPF($this->input->post('cpf'));

        if ($dados === NULL)
        {
            echo 'Nao retorno nada';
        }
        else
        {
            $this->load->view('admin/view/head_script', $dados);
            $this->load->view('user/registro_view');
            $this->load->view('admin/view/footer_script');
        }

    }
    
    public function registrarPonto($cpf=NULL)
    {
        //Verifica se foi passado o campo nome vazio.
        if ($this->input->post('cpf') == NULL)
        {
            echo 'O compo CPF é obrigatório.';
        }
        else 
        {
            //Carrega o Model usuarios
            $this->load->model('admin/Usuarios_model', 'usuarios');

            //Pega dados do post e guarda na array $dados
            $dados['cpf']        = $this->input->post('cpf');
            $dados['nome']       = $this->input->post('nome');
            $dados['nascimento'] = $this->input->post('nascimento');
            $dados['horario']    = $this->input->post('horario');

            //Se não foi passado id ele adiciona um novo registro
            if ($dados != NULL)
            {
                $this->usuarios->addRegistro($dados);
            }

            redirect(base_url('registrar/'), 'refresh');
        }

            //redicionamento para a página 
            /* redirect('http://localhost:8888/sigaregistroo/usuario'); */
    }

    public function registrar()
    {
        $this->load->view('admin/view/head_script');
        $this->load->view('user/registrar_view');
        $this->load->view('admin/view/footer_script');
    }
}
