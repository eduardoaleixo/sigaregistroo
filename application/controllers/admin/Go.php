 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Go extends CI_Controller {

	public function index()
	{
        $this->listarUsuarios();
    }
    
    public function listarUsuarios() {

        $this->load->model('admin/Usuarios_model', 'usuarios');
        
        $dados['usuarios'] = $this->usuarios->listarUsuarios();
        
        $this->load->view('admin/view/head_script', $dados);
        $this->load->view('admin/listar_view');
        $this->load->view('admin/view/footer_script');
    }

    public function adicionarUsuario()
    {
        $this->load->view('admin/view/head_script');
        $this->load->view('admin/usuario_view');
        $this->load->view('admin/view/footer_script');
    }

    public function salvarUsuario()
	{
		//Verifica se foi passado o campo nome vazio.
		if ($this->input->post('nome') == NULL)
		{
			echo 'O compo nome do usuario é obrigatório.';
			echo 'Voltar';
		} 
		else 
		{
			//Carrega o Model usuarios				
			$this->load->model('admin/Usuarios_model', 'usuarios');

			//Pega dados do post e guarda na array $dados
			$dados['nome'] = $this->input->post('nome');
			$dados['cpf'] = $this->input->post('cpf');
			$dados['setor'] = $this->input->post('setor');		
			$dados['nascimento'] = $this->input->post('nascimento');		
			$dados['status'] = $this->input->post('status');		

			//Verifica se foi passado via post a id do usuarios
			if ($this->input->post('id') != NULL) {		
				//Se foi passado ele vai fazer atualização no registro.	
				$this->usuarios->editarUsuario($dados, $this->input->post('id'));

				redirect(base_url('/'),'refresh');
			} 
			
			if ($this->input->post('cpf') != NULL)
			{
				$dados['usuarios'] = $this->usuarios->cpfExiste($this->input->post('cpf'));
				
				$this->load->view('admin/view/head_script');
				$this->load->view('admin/errors/cpfExiste_view');
				$this->load->view('admin/view/footer_script');
			}

			
			else {
				//Se Não foi passado id ele adiciona um novo registro
				$this->usuarios->addUsuario($dados);
				redirect('/');
			}

			//redicionamento para a página 		
			/* redirect('http://localhost:8888/sigaregistroo/listar'); */
			$this->load->view('admin/view/head_script', $dados);
			$this->load->view('admin/listar_view');
			$this->load->view('admin/view/footer_script');	
			
		}
    }

    //Página de editar usuario
    public function editarUsuario($id=NULL)	
    {
        //Verifica se foi passado um ID, se não vai para a página listar usuarios
		if($id == NULL) 
		{
			echo 'Nenhum usuário registrado.';
        }

        //Carrega o Model usuarios				
		$this->load->model('admin/Usuarios_model', 'usuarios');

        //Faz a consulta no banco de dados pra verificar se existe
        $query = $this->usuarios->retornoUsuarioID($id);

        //Verifica que a consulta voltar um registro, se não vai para a página listar usuarios
		if($query == NULL) 
		{
			echo 'Como assim não encontrou nenhum usuário?';	
		}

		//Criamos uma array onde vai guardar os dados do usuario e passamos como parametro para view;	
		$dados['usuario'] = $query;

		//Carrega a View
		$this->load->view('admin/view/head_script', $dados);
		$this->load->view('admin/editarUsuario_view');
		$this->load->view('admin/view/footer_script');
    }

    public function apagarUsuarioGo($id=NULL)
	{
		//Verifica se foi passado um ID, se não vai para a página listar usuarios
		if($id == NULL) {
			redirect('/');
		}

		//Carrega o Model usuarios				
		$this->load->model('admin/Usuarios_model', 'usuarios');

		//Faz a consulta no banco de dados pra verificar se existe
		$query = $this->usuarios->retornoUsuarioID($id);

		//Verifica se foi encontrado um registro com a ID passada
		if($query != NULL) {
			
			//Executa a função apagarusuarios do usuarios_model
			$this->usuarios->apagarUsuario($query->id);
			redirect('/');

		} else {
			//Se não encontrou nenhum registro no banco de dados com a ID passada ele volta para página listar usuarios
			redirect('/');
		}
	}

	public function buscar($nome = NULL)
	{
		$this->load->model('admin/Usuarios_model', 'usuarios');

        $dados['usuarios'] = $this->usuarios->buscarUsuario($this->input->post('nome'));

        $this->load->view('admin/view/head_script', $dados);
        $this->load->view('admin/buscar_view');
        $this->load->view('admin/view/footer_script');
	}

	public function usuariosDesativados()
    {
        $this->load->model('admin/Usuarios_model', 'usuarios');

        $dados['usuarios'] = $this->usuarios->listarUsuarios();

        $this->load->view('admin/view/head_script', $dados);
        $this->load->view('admin/usuariosDesativados_view');
        $this->load->view('admin/view/footer_script');
    }
}
