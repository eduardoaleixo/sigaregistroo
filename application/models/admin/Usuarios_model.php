<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios_model extends CI_model {

    public function listarUsuarios() 
    {
        $get = $this->db->get('usuarios');
        return $get->result();
    }


    /* public function addUsuario($dados=NULL)
    {
        $this->db->where('cpf', $cpf);
        $this->db->get('usuarios');
        $registros = $this->db->count_all_results();

        if ($registros > 0)
        {
        
        } 
        else 
        {
            $this->db->insert('usuarios', $dados);
        }
    } */

    public function addUsuario($dados=NULL)
    {
        if ($dados != NULL)
        {
            $this->db->insert('usuarios', $dados);
        }
    }

    public function cpfExiste($cpf = NULL)
    {
        $this->db->where('cpf', $cpf);
        $get = $this->db->get('usuarios');
        return $get->result();
    }

    public function retornoUsuarioID($id=NULL)
    {
            if ($id != NULL){
            {
                //Verifica se a ID no banco de dados
                $this->db->where('id', $id);
                //limita para apenas um registro    
                $this->db->limit(1);
                //pega os usuarios
                $query = $this->db->get("usuarios");
                //retorna o usuario para o controller
                return $query->row();   

                $query = $this->db->get_where('usuarios', array('id' => $id), '$1');
            }
        } 
    }

    public function editarUsuario($dados=NULL, $id=NULL)
    {
        //Verifica se foi passado $dados e $id    
        if ($dados != NULL && $id != NULL){
            //Se foi passado ele vai para atualização
            $this->db->update('usuarios', $dados, array('id'=>$id));      
        }
    }

    public function apagarUsuario($id=NULL)
    {
        //Verificamos se foi passado o a ID como parametro
        if ($id != NULL)
        {
            $this->db->delete('usuarios', array('id'=>$id));            
        }
    }


//--------------------------REGISTRO DE PONTO--------------------------//

public function usuarioCPF($cpf=NULL) //consulta o CPF do usuário la tabela USUARIOS
    {
        $this->db->where('cpf', $cpf);
        $this->db->limit(1);
        $query = $this->db->get('usuarios', 1);
        return $query->row();   
        $query = $this->db->get_where('usuarios', array('cpf' => $cpf));
        /* return $query->result(); */
    }

    public function addRegistro($dados=NULL)
    {
        if ($dados != NULL)
        {
            $this->db->insert('registros', $dados);
        }
    }

    public function atualizarRegistro($dados=NULL, $cpf=NULL)
    {
        //Verifica se foi passado $dados e $cpf
        if ($dados != NULL && $cpf != NULL){
            //Se foi passado ele vai para atualização
            $this->db->update('registros', $dados, array('cpf'=>$cpf));
        }
    }

//--------------------------TESTE--------------------------//

    public function buscarUsuario($nome = NULL)
    {
        $this->db->where('nome', $nome);
        $get = $this->db->get('registros');
        return $get->result();
    }
}